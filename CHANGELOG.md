# CHANGELOG for mbusc

Home: https://gitlab.com/groengemak/mbusc

## v0.1 (2022-10-28)

* Initial release
* Able to poll via Modbus TCP, store in RRD
* Suggest use with mbusd
* Build against libev, libinih
* Imported a few ARPA2 goodies by hand
