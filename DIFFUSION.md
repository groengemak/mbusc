# Diffusion Models

> *Many systems will transport energy to overcome a difference.
> This process is called diffusion.  Only when inertia also plays
> a role, might systems resonante.*

Thermal systems tend to move to a future target value with an
exponential drop; that is, they halve the difference between
the current and target temperature in a fixed time.  The target
will shift from time to time, but between that the drop is a
possible source for the target temperature that would be reached
when no other parts of the system change.

A differential of an exponent (also with negative exponent, and
also with a fixed offset) will again be an exponent (multiplied
by a constant) with zero/no offset.  So now we have a related
exponent that will always drop to 0.  Every two measurements now
supplies a new estimate of the dropping speed, and in a sequence
it would be possible to recognise a trend and, more interestingly,
trend changes.

Two differentiated samples with a fixed time distance have a fixed
ratio within a trend.  Taking the logarithm of this trend makes it
uniformly distributed (TODO:CHECK) so it may be assumed to have a
normal distribution.  The more of a trend is accumulated, the
smaller σ will be.  At some point, a trend is broken, and the
certainty can be derived from the number of σ from μ.  Especially
a few such consecutive break-outs helpt to learn about a new trend.
This should make it possible to detect trend breaks quickly, in a
matter of a few sample periods, and start measuring from scratch.

The actual learning speed, detected as the ratio or its logarithm,
will differ between sources.  An open door or window has another
impact than a heater nearby, or remote.

