# Model for mbusc

> *This is the computational model for Modbus control.
> It is designed for tempature control, but should work
> for many other linear models.*

The assumptions made in this model are:

**Linearity.**
The `unit` is part of a time-invariant linear model.
That is, it is chosen such that it makes sense to assume linearity
to find a model.  For some areas, this may require mapping the
values to such a model for the purposes of modeling.

Many aspects of nature demonstrate exponential growth or decay.
Exponential decay is usually the result of a difference driving
energy away from a process; exponential growth is usually the
result of a growth by a factor that repeats over time.  Looking
at the time-invariant models however, these systems are linear.

**Orthogonality.**
The model derives outputs from inputs using a covariance matrix.
There is a risk of finding multiple relationshsips, in which case
an output would be wrongly estimated.

This only becomes a problem when the driving forces are not
orthogonal; there is no problem when multiple measurements respond
to the same driving force.  So, there must be orthogonality for
the controlling elements, even when they may be distributed in a
linear manner over multiple devices.

**Delays.**
The model may be time-invariant, but it can still benefit from
knowing what happened in the past.  That is because there may be
a delay in delivering energy, such as heat that dissipates only
slowly.  This may be handled by introducing past control driving
as a trigger for current changes.

Like in Holt-Winters prediction, there is an order in which the
factors influence the analysed behaviour, namely from new to old.
It is a useful idea to first learn from the latest control action
and only continue to the next after subtracting it.

Delayed actions usually reduce in intensity when time advances,
so the impact of delayed information can be maintained much more
efficiently by looking at doubling delay times.  (TODO:IDEA).
Given the time-ordered learning effect, the learning would only
be done for remainders that were not yet explained by past values.
It may be helpful to collect remaining historic values in likewise
delayed moments.  With a set schedule, it would be possible to
compute such delayed effects only once in a while.

Forward calculation of delayed effects would be spread over more
time, and then compensate for the stronger effect.

*Note:* This may work for dissipating effects, but not for ones
that show resonance.  In that sense, this is a heat-specific
model.  Noise reduction is an example that needs the full resolution.
Although even there the element of exponential volume decay plays
a role.

**Learning.**
The learning process starts with a prediction of measurements,
determines the difference with their actual value and sends back
corrections to the covariances with (orthogonal) drivers.  (TODO:DETAILS)
Corrections are learned through exponential weighted averaging.
(TODO:MULTIDIM)

Since the model is linear, it is possible to work on differences,
which are then added back in.  This is helpful to handle opposite
or additional influences of the various controlled drivers.  Since
they are orthogonal, at least inasfar as they are passed into the
model, they can each learn independently.

