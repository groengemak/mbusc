/* rrd.c -- Write Modbus measurements to RRD cyclical archives.
 *
 * RRDs are written with rrdupdate.  Is it wasteful to run a command
 * for every sample?  Yes.  No.  Maybe.  In fact, one invocation can
 * insert multiple values.  But those are usually separated in time
 * and so a crash halfway might drop values.
 *
 * The solution is to run rrdupdate, with all its general arguments,
 * within xargs, which then collects the data line by line, and adds
 * as many to a commandline as will fit, to run rrdupdate ones as a
 * larger update.  Low on processor resources, forking activity and
 * disk I/O.  Gotta love it.  Oh, and the best of all, xargs reads
 * from a pipe and detects if it is closed.  When run in a separate
 * session, it will not crash and die along with the Modbus client,
 * and can quietly process any pending data.  As long as data lines
 * are written atomically, there will be no confusion.
 *
 * Taraaaa :)
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include <sys/types.h>
#include <unistd.h>

#include <arpa/inet.h>		/* for ntohs() */

#include "bus.h"
#include "rrd.h"


/* Data structure for storing an RRD data element.
 *
 * This uses an opaque type defined in "bus.h" for RRD use.
 * The pointer must be setup by this file to make that work.
 */
struct respdata_t {
	//
	// Linked list of elements in the order presented to rrdpipe()
	struct rrdata *next;
	//
	// The variable name
	char *name;
	//
	// Scale and offset to map the raw value to a readout
	double scale, offset;
	//
	// Accumulator, or last value, whichever applies
	union {
		uint64_t i;
		double d;
	} accu, reset;
	//
	// Whether this value changed since it was last written
	bool fresh;
	//
	// Update processing: int/double, and whether to add
	bool int_not_double;
	//bool add_value;
};


/* Open a pipeline to write samples into an RRD.  This starts a
 * process that reads lines through xargs, and places them on an
 * rrdupdate commandline.  This is run in a separate session, so
 * its signals are separated from this program.
 *
 * The varnames is a NULL-terminated variable name sequence,
 * in the order in which values will be supplied.
 *
 * Writing into the returned pipe can be done with vdprintf(),
 * writing colon-separated timestamp and the values corresponding
 * to varnames, followed by a newline.  Do not use N for "now" or
 * relative at-timestamps.
 *
 * The maxdelay value indicates how many sample periods it may
 * take until data makes it into the RRD.  Set to 0 it triggers
 * instant delivery.
 *
 * On error, this function returns -1 and sets errno.  Success
 * returns a file descriptor >= 0.
 */
int rrdpipe (const char *rrdfilename, const char **varnames, unsigned maxdelay) {
	//
	// Form the template
	int templatelen = 1;
	for (const char **var = varnames; *var; var++) {
		templatelen += 1 + strlen (*var);
	}
	char template [templatelen];
	template [0] = '\0';
	for (const char **var = varnames; *var; var++) {
		if (var != varnames) {
			strcat (template, ":");
		}
		strcat (template, *var);
	}
	//
	// Turn maxdelay into a (sensible) configuration option
	char delaycount [20];
	sprintf (delaycount, "--max-args=%u",
				maxdelay < 86400 ? maxdelay + 1 : 86400);
	//
	// Form the commandline
	char *xargs = "/usr/bin/xargs";
#if DEBUG
	char *update = "updatev";
#else
	char *update = "update";
#endif
	char *cmdline [] = { xargs,
					"--delimiter=\\n",
					"--no-run-if-empty",
					delaycount,
				"rrdtool", update,
					(char *) rrdfilename,
					"--template", template,
				NULL };
	//
	// Create a pipe between client and server
	int cmdpipe [2];
	if (pipe (cmdpipe) != 0) {
		return -1;
	}
	//
	// Fork the child process and set it up
	pid_t pid = fork ();
	switch (pid) {
	case -1:
		/* An error occurred */
		close (cmdpipe [0]);
		close (cmdpipe [1]);
		return -1;
	case 0:
		/* Child process */
		dup2  (cmdpipe [0], 0);
		close (cmdpipe [0]);
		close (cmdpipe [1]);
		printf ("Client started in pid %d\n", getpid ());
		setsid ();
		execv (xargs, cmdline);
		exit (1);
	default:
		/* Parent process */
		close (cmdpipe [0]);
		return cmdpipe [1];
	}
}


/* Extract an integer value from registeres.
 */
static int64_t raw_i16 (uint8_t *data) {
	//
	// Extract the raw integer frmo the first register
	int raw = (int) ntohs (* (uint16_t *) &data[1]);
	//
	// Map to int64_t and return
	return (int64_t) raw;
}


/* Standard hook for a sample from an u16 register.
 * Assumes that respdata is setup for this module's interpretation.
 */
void rrdhook_sample_i16 (struct cmdio *cmd, double period, uint8_t *data) {
	//
	// Extract the raw  value
	int16_t raw = * (int16_t *) &data [1];
	raw = ntohs (raw);
	//
	// Map the raw register value with offset and scale
	double value = (raw + cmd->respdata->offset) * cmd->respdata->scale;
	//
	// Write the value
	if (cmd->respdata->int_not_double) {
		cmd->respdata->accu.i = (int64_t) value;
	} else {
		cmd->respdata->accu.d = value;
	}
	//
	// Mark the value as fresh
	cmd->respdata->fresh = true;
}


/* Standard hook for a time-integrated flow value; it multipies by the period.
 * A typical example would be to map power samples to energy readings.
 * Assumes that respdata is setup for this module's interpretation.
 */
void rrdhook_flow_u16 (struct cmdio *cmd, double period, uint8_t *data) {
	//
	// Extract the raw value
	int16_t raw = * (int16_t *) &data [1];
	raw = ntohs (raw);
	//
	// Map the raw register value with offset and scale and period
	// The value will be split in before/after value, through reset
	double value = (raw + cmd->respdata->offset) * cmd->respdata->scale * period / 2.0;
	//
	// Add to the stored value
	if (cmd->respdata->int_not_double) {
		/* For integers, round accu up and reset down */
		cmd->respdata->accu.i += cmd->respdata->reset.i + (int64_t) (value + 0.5);
		cmd->respdata->reset.i = (int64_t) value;
	} else {
		cmd->respdata->accu.d += cmd->respdata->reset.d + value;
		cmd->respdata->reset.d = value;
	}
	//
	// Mark the value as fresh
	cmd->respdata->fresh = true;
}


