/* Modbus TCP or RTU interface for event-driving polling.
 *
 * The cmdio_add() and cmdio_del() can be used to add cmdio structures,
 * to setup commands that are sent at a regular beat.  Run cmdio_loop()
 * to actually start this procedure.  Each cmdio structure defines a
 * response callback hook (respcb) that allows the processing of data.
 *
 * The structures below consider address and data, but the data[] is
 * an abstract byte array to be setup and parsed in the callback hooks.
 * The CRC for Modbus RTU mode is concealed, as is the TCP header for
 * Modbus TCP.  Request and response length should be set to the length
 * of the data[] byte array.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <ev.h>



/* When Modbus RTU is disabled, then Modbus TCP must be enabled.
 */
#ifndef SUPPORT_MODBUS_RTU
#define SUPPORT_MODBUS_TCP
#endif


/* The response hook is called when a response arrives to a scheduled
 * request.  The response is of the size of the requested resplen, and
 * the pointer to its data is passed in a buffer that may only be
 * assumed stable during this callback.  Further data is provided in
 * the cmdio structure.
 */
struct cmdio;
typedef void (*response_hook_t) (struct cmdio *cmd, double period, uint8_t *data);


/* An abstract structure for response data, to be defined in the
 * response hook handler module.  A pointer to this data is added
 * to the cmdio structure to facilitate data references for the
 * response_hook_t to enjoy.  When handled well, it would even
 * work to use different types in different modules, all using
 * the name of respdata_t.
 */
struct respdata_t;


/* Type information in 32 bits: regtype and rrdtype combined:
 *
 *  - The array size-1 (encoding 0..256) over the base type
 *  - The base type: UINT, INT, FLOAT, BIT or FIELD
 *  - Numeric size: 16, 32 or 64 bits
 *  - For base type FIELD, the HIGH:LOW values
 *  - Flags:
 *     - LENDIAN for little-endian register order
 *     - FLOW for integration over time
 *
 * The value 0 is the default: u16
 */

//
// The number of array elements (may be used for bits)
// Uses 0 for no array variant (so just the register)
#define TYPE_ARRAY_MASK   0x000007ff

//
// Base types for the regtype
#define TYPE_BASE_MASK    0x0000f000
#define TYPE_BASE_UINT    0x00000000
#define TYPE_BASE_INT     0x00001000
#define TYPE_BASE_FLOAT   0x00002000
#define TYPE_BASE_BIT     0x00003000
#define TYPE_BASE_FIELD   0x00007000

//
// For _UINT, _INT, _FLOAT, the size (1 below #words)
#define TYPE_SIZE_SHIFT 16
#define TYPE_SIZE_MASK    0x000f0000
#define TYPE_SIZE_NA      0x00000000
#define TYPE_SIZE_16      0x00000000
#define TYPE_SIZE_32      0x00010000
#define TYPE_SIZE_64      0x00030000

//
// Low and high bit numbers for TYPE_BASE_FIELD
#define TYPE_LOW_SHIFT  20
#define TYPE_HIGH_SHIFT 24
#define TYPE_LOW_MASK     0x00f00000
#define TYPE_HIGH_MASK    0x0f000000

//
// Flags for this type
//  - TYPE_FLAG_FLOW for as a time-integrated flow measurement
//  - TYPE_FLAG_LENDIAN for little-endian register order per _SIZE_
//  - TYPE_FLAG_INVERT for logical inversion of all the bits
#define TYPE_FLAG_MASK    0xf0000000
#define TYPE_FLAG_LENDIAN 0x10000000
#define TYPE_FLAG_FLOW    0x20000000
#define TYPE_FLAG_INVERT  0x40000000


/* Command structures are used to store a standard command and help the
 * scheduler to poll at a regular pace, and to callback a hook when a
 * response arrives.
 *
 * Some behaviour in this module depend on SUPPORT_MODBUS_TCP and _RTU,
 * but the structures below are always the same.  This allows separate
 * compilation of this work into a library, with a version-fixed API.
 */
struct cmdio {
	//
	// The periodic timer to control regular sending of this command.
	ev_periodic _poll_timer;
	//
	// The response message length (address, command, data...).
	//
	// Modbus TCP uses no CRC, but if this is ever used for Modbus RTU then
	// the CRC would be included in the length and message.
	//
	uint16_t resplen;
	//
	// The callback hook for an incoming response to this request.
	// This is supplied with this structure, which is why there also is
	// a private respdata field contained herein.
	//
	response_hook_t respcb;
	struct respdata_t *respdata;
	//
	// Generic handling combines the regtype and rrdtype to datatype.
	// See the values for TYPE_xxx in "reg.h" and RRD_xxx in "rrd.h"
	uint32_t datatype;
	//
	// The request length and its message bytes (address, command, data...)
	// with data bytes continuing after this structure.  In addition, the
	// prefix fields for Modbus TCP, even the zero bytes for proto.
	//
	// The structure should add as many bytes as it wants to send in data,
	// so reqlen-2 extra bytes.  The fact that 2 bytes are reserved is not
	// for the (address, command, data...) fields but for a CRC that may be
	// added if this is run over Modbus RTU.  These fields are meaningless
	// to Modbus TCP, but these pre-allocated CRC bytes allow that to be
	// hidden behind an API.  Any CRC will be computed when adding the
	// cmdio structure for transmission; the reqlen and resplen may also
	// be changed differently by the two modes; do not worry but just think
	// of the (address, command, data...) sequence while using cmdio.
	//
	uint8_t txnid_h, txnid_l;
	uint8_t proto_h, proto_l;
	uint8_t reqlen_h, reqlen_l;
	uint8_t address;
	uint8_t command;
	uint8_t data [2];
};


/* The CMDIO_VERSION value is passed along with the cmdio structure to set its
 * interpretation.  This can be helpful to protect a library from separately
 * compiled programs that make other assumptions.  The trick is to use the
 * cmdio_version value in calls to the library, which may or may not know
 * what to do.  The callback will also assume this same version.
 */
typedef enum {
	cmdio_v0 = 0,
	// cmdio_v1 = ...
	cmdio_version = 0
} cmdio_version_t;


/* Use SET_CMDIO() to setup a previously allocated (struct cmdio *)
 * with the desired command data.
 */
#define SET_CMDIO(ptrname,addr,cmd,reqdatalen,respdatalen) \
			ptrname->proto_h = 0x00; ptrname->proto_l = 0x00; \
			ptrname->address = (addr); ptrname->command = (cmd); \
			ptrname->reqlen_h = ((2+(reqdatalen)) >> 8); ptrname->reqlen_l = ((2+(reqdatalen)) & 0xff); \
			ptrname->resplen = 2 + (respdatalen)


/* Use NEW_CMDIO() to allocate a (struct cmdio *) in a stack variable
 * of a given name, and continue into SET_CMDIO() for filling its data.
 */
#define NEW_CMDIO(name,addr,cmd,reqdatalen,respdatalen) \
			uint8_t *_ ## name [sizeof(struct cmdio) + (reqdatalen)]; \
			struct cmdio *name = (struct cmdio *) _ ## name; \
			SET_CMDIO (name,addr,cmd,reqdatalen,respdatalen)


/* Use ALLOC_CMDIO() to allocate a (struct cmdio *) on the heap pointed to
 * by a predefined name, and continue into SET_CMDIO() for filling its data.
 * The errjmp will be jumped to with goto if allocation fails.
 */
#define ALLOC_CMDIO(name,errjmp,addr,cmd,reqdatalen,respdatalen) \
			(name) = (struct cmdio *) calloc (1, sizeof (struct cmdio) + (reqdatalen)); \
			if ((name) == NULL) { goto errjmp; } \
			SET_CMDIO (name,addr,cmd,reqdatalen,respdatalen)


/* Add a cmdio structure to the polling loop, to be used forevermore.
 * The version to interpret is provided by the calling module, and should
 * be set to cmdio_version.
 *
 * The start is the time at which the RRD polling started, and it indicates
 * a good time to start polling (modulo the period).  The next offset will
 * be determined, and the period then continues a regular poll.  This aligns
 * as well as possible with the RRD timing.
 *
 * Return true on success.  On failure, set errno and return false.
 */
bool cmdio_add (struct cmdio *addend, cmdio_version_t interp, time_t start, double period);


/* Remove a cmdio structure from the polling loop.
 */
bool cmdio_del (struct cmdio *goner, cmdio_version_t interp);


/* Run a Modbus loop over the given file descriptor; with rtu set,
 * this is considered a serial bus for Modbus RTU, otherwise it is
 * considered a socket for Modbus TCP.
 *
 * This function may run indefinitely, but it will return when an
 * unrecoverable error occurs, like memory errors or communication
 * problems.  EAGAIN/EWOULDBLOCK do not count as unrecoverable.
 *
 * Returns true if *something* was sent, false otherwise.
 * Always sents errno, check this before looping around.
 *
 * It is possible to call this again with the same or new Modbus
 * coordinates.  Everything setup with cmdio_add() is still in
 * the event loop, and anything queued still is.  The work can
 * just continue; this is just what you want for Modbus RTU, and
 * it is not usually damaging for Modbus TCP.
 */
bool cmdio_loop (int fd, bool rtu);

