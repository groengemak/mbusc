/* rrd.h -- Write Modbus measurements to RRD cyclical archives.
 *
 * RRDs are written with rrdupdate.  Is it wasteful to run a command
 * for every sample?  Yes.  No.  Maybe.  In fact, one invocation can
 * insert multiple values.  But those are usually separated in time
 * and so a crash halfway might drop values.
 *
 * The solution is to run rrdupdate, with all its general arguments,
 * within xargs, which then collects the data line by line, and adds
 * as many to a commandline as will fit, to run rrdupdate ones as a
 * larger update.  Low on processor resources, forking activity and
 * disk I/O.  Gotta love it.  Oh, and the best of all, xargs reads
 * from a pipe and detects if it is closed.  When run in a separate
 * session, it will not crash and die along with the Modbus client,
 * and can quietly process any pending data.  As long as data lines
 * are written atomically, there will be no confusion.
 *
 * Taraaaa :)
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */



/* Open a pipeline to write samples into an RRD.  This starts a
 * process that reads lines through xargs, and places them on an
 * rrdupdate commandline.  This is run in a separate session, so
 * its signals are separated from this program.
 *
 * The varnames is a NULL-terminated variable name sequence,
 * in the order in which values will be supplied.
 *
 * Writing into the returned pipe can be done with vdprintf(),
 * writing colon-separated timestamp and the values corresponding
 * to varnames, followed by a newline.  Do not use N for "now" or
 * relative at-timestamps.
 *
 * The maxdelay value indicates how many sample periods it may
 * take until data makes it into the RRD.  Set to 0 it triggers
 * instant delivery.
 *
 * On error, this function returns -1 and sets errno.  Success
 * returns a file descriptor >= 0.
 */
int rrdpipe (const char *rrdfilename, const char **varnames, unsigned maxdelay);


