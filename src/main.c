/* main.c -- Modbus Client main program
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include <time.h>

#include <arpa/inet.h>		/* for ntohs() */

#include "bus.h"
#include "rrd.h"
#include "conf.h"

#include <arpa2/socket.h>


int rp = -1;


void hook_on (struct cmdio *cmd, double period, uint8_t *data) {
	printf ("Device switched on:  %02x %02x %02x %02x\n",
			data[0], data[1], data[2], data[3]);
}


void hook_off (struct cmdio *cmd, double period, uint8_t *data) {
	printf ("Device switched off: %02x %02x %02x %02x\n",
			data[0], data[1], data[2], data[3]);
}


void hook_temp (struct cmdio *cmd, double period, uint8_t *data) {
	uint16_t *data16 = (uint16_t *) &data [1];
	printf ("Device temperatures: %d %d %d %d\n",
			(int) ntohs (data16 [0]),
			(int) ntohs (data16 [1]),
			(int) ntohs (data16 [2]),
			(int) ntohs (data16 [3]));
}

void hook_moist (struct cmdio *cmd, double period, uint8_t *data) {
	uint16_t *data16 = (uint16_t *) &data [1];
	float temp  = ntohs (data16 [0]) / 10.0;
	float moist = ntohs (data16 [1]) / 10.0;
	printf ("Temperature %f, moist %f%%\n", temp, moist);
	time_t now = time (NULL);
	dprintf (rp, "%lu:%f:%f\n", (unsigned long) now, temp, moist);
}

void hook_pzem (struct cmdio *cmd, double period, uint8_t *data) {
	uint16_t *data16 = (uint16_t *) &data [1];
	uint16_t voltage   = ntohs (data16 [0]);
	uint32_t current   = ntohs (data16 [1]) + 65536 * ntohs (data16 [2]);
	uint32_t power     = ntohs (data16 [3]) + 65536 * ntohs (data16 [4]);
	uint32_t energy    = ntohs (data16 [5]) + 65536 * ntohs (data16 [6]);
	uint16_t frequency = ntohs (data16 [7]);
	uint16_t powerfact = ntohs (data16 [8]);
	bool alarmstate = (ntohs (data16 [9] != 0x0000));
	printf ("U=%5.1f V, I=%5.1f A, P=%5.1f W, E=%5.1f Wh, f=%5.1f Hz, cos(φ)=%5.1f\n",
			voltage * 0.1, current * 0.001,
			power * 0.1, energy * 1.0,
			frequency * 0.1, powerfact * 0.01);
}


/* TODO: GENERIC HOOK:
 *  1. Read  according to the regtype (possibly multiply by time/period)
 *  2. Write according to the rrdtype
 */
void hook_generic_int (struct cmdio *cmd, double period, uint8_t *data) {
	//
	// Read the value as setup in the regtype
	"TODO";
	//
	// Write according to the rrdtype, modify for time-integral flows
	"TODO";
}


/* TODO: GENERIC HOOK:
 *  1. Read  according to the regtype (possibly multiply by time/period)
 *  2. Write according to the rrdtype
 */
void hook_generic_float (struct cmdio *cmd, double period, uint8_t *data) {
	//
	// Read the value as setup in the regtype
	"TODO";
	//
	// Write according to the rrdtype, modify for time-integral flows
	"TODO";
}


/* TODO: GENERIC HOOK:
 *  1. Read  according to the regtype (possibly multiply by time/period)
 *  2. Write according to the rrdtype
 */
void hook_generic_bits (struct cmdio *cmd, double period, uint8_t *data) {
	//
	// Read the value as setup in the regtype
	"TODO";
	//
	// Write according to the rrdtype, modify for time-integral flows
	"TODO";
}



/* Load configuration elements and add them to the event loop for the bus.
 */
void schedule_conffile (const char *filename) {
	struct conf *settings = conf_read (filename);
	if (settings == NULL) {
		fprintf (stderr, "Nothing found in configfile %s\n", filename);
		exit (1);
	}
	bool sth_ok = false;
	for (struct conf *this = settings;
				(this != NULL) && (this->previous != NULL);
				this = this->previous) {
		bool ok = conf_cmdio_add (this);
		if (!ok) {
			fprintf (stderr, "Section %s did not launch properly\n", settings->var [CONF_SECTION]);
		} else {
			fprintf (stderr, "DEBUG: Section %s launched\n", settings->var [CONF_SECTION]);
			sth_ok = true;
		}
	}
	if (!sth_ok) {
		fprintf (stderr, "Nothing worked.  That's a fatal problem.\n");
		exit (1);
	}
}


int main (int argc, char *argv []) {
	//
	// Connect to the server
	int sox = -1;
	struct sockaddr_storage ss;
	bool ok = true;
	socket_init ();
	ok = ok && socket_parse ("::1", "5020", &ss);		/* TODO:FIXED */
	ok = ok && socket_client (&ss, SOCK_STREAM, &sox);
	ok = ok && socket_nonblock (sox);
	if (!ok) {
		fprintf (stderr, "Failed to connect to Modbus TCP server\n");
		goto bailout;
	}
	printf ("Connected.  Me Happy.\n");
	//
	// Create a pipeline for the RRD
	const char *vars [] = { "temp", "humid", NULL };
	rp = rrdpipe ("/tmp/test.rrd", vars, 9);
	if (rp < 0) {
		fprintf (stderr, "Failed to open RRD pipeline\n");
		goto bailout_cnx;
	}
	//
	// Construct cmdio structures for polling commands
#if 0
	NEW_CMDIO (on, 0x03, 0x06, 4, 4);
	on->data [0] = 0x00;
	on->data [1] = 0x35;
	on->data [2] = 0x00;
	on->data [3] = 0x01;
	on->respcb = hook_on;
	cmdio_add (on, cmdio_version, 10, 60);
	NEW_CMDIO (off, 0x03, 0x06, 4, 4);
	off->data [0] = 0x00;
	off->data [1] = 0x35;
	off->data [2] = 0x00;
	off->data [3] = 0x00;
	off->respcb = hook_off;
	cmdio_add (off, cmdio_version, 0, 60);
#endif
#if 0
	NEW_CMDIO (temp, 0x03, 0x03, 4, 11);
	temp->data [0] = 0x00;
	temp->data [1] = 0x32;
	temp->data [2] = 0x00;
	temp->data [3] = 0x04;
	temp->respcb = hook_temp;
	cmdio_add (temp, cmdio_version, 2, 60);
#endif
#if 0
	NEW_CMDIO (moist, 0x01, 0x04, 4, 5);
	moist->data [0] = 0x00;
	moist->data [1] = 0x01;
	moist->data [2] = 0x00;
	moist->data [3] = 0x02;
	moist->respcb = hook_moist;
	cmdio_add (moist, cmdio_version, 0, 60);
#endif
#if 1
	//NAH// NEW_CMDIO (pzem, 0x10, 0x04, 4, 21);
	//OOKNIEJ// NEW_CMDIO (pzem, 0x03, 0x04, 4, 21);
	NEW_CMDIO (pzem, 0x01, 0x04, 4, 21);
	pzem->data [0] = 0x00;
	pzem->data [1] = 0x00;
	pzem->data [2] = 0x00;
	pzem->data [3] = 0x0a;
	pzem->respcb = hook_pzem;
	cmdio_add (pzem, cmdio_version, 0, 5);
#endif
	//
	// Parse a configuration, if one is given
	if (argc >= 2) {
		schedule_conffile (argv [1]);
	}
	//
	// Enter the event loop for polling
	printf ("Entering loop.  Should exit when mbusd disconnects.\n");
	cmdio_loop (sox, false);
	//
	// Close down and cleanup
	printf ("Closing.\n");
bailout_rrd:
	close (rp);
bailout_cnx:
	socket_close (sox);
bailout:
	socket_fini ();
}

