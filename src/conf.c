/* conf.c -- Configuration parser based on INI files with special fields.
 *
 * The configuration file format is documented elsewhere, but comes down
 * to [section]s with var = value string settings.  Some of the value strings
 * may be further broken down to flags or other values.  Some var names
 * may have default value strings, and a generic facility for that is in
 * the global part, preceding the first [section].
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include <ini.h>

#include "bus.h"
#include "conf.h"



/* Static map of variable names to variable indexes and optional
 * detail parsers.
 */
typedef bool (*detailparser_t) (struct conf *confio, const char *valstr);
//
static bool confparse_offset	(struct conf *confio, const char *valstr);
static bool confparse_scale	(struct conf *confio, const char *valstr);
static bool confparse_range	(struct conf *confio, const char *valstr);
static bool confparse_regtype	(struct conf *confio, const char *valstr);
static bool confparse_input	(struct conf *confio, const char *valstr);
static bool confparse_output	(struct conf *confio, const char *valstr);
//
struct confwords {
	const char *name;
	enum confvar index;
	detailparser_t detailparser;
};
//
struct confwords confwords [] = {
	{ "name",	CONF_NAME,	NULL			},
	{ "descr",	CONF_DESCR,	NULL			},
	{ "unit",	CONF_UNIT,	NULL			},
	{ "offset",	CONF_OFFSET,	confparse_offset	},
	{ "scale",	CONF_SCALE,	confparse_scale		},
	{ "range",	CONF_RANGE,	confparse_range		},
	{ "server",	CONF_SERVER,	NULL			},
	{ "port",	CONF_PORT,	NULL			},
	{ "regtype",	CONF_REGTYPE,	confparse_regtype	},
	{ "input",	CONF_INPUT,	confparse_input		},
	{ "output",	CONF_OUTPUT,	confparse_output	},
	// { "compute",	CONF_COMPUTE,	NULL			},
	// { "graph",	CONF_GRAPH,	NULL			},
	// { "export",	CONF_EXPORT,	NULL			},
	{ "rrdfile",	CONF_RRDFILE,	NULL			},
	{ "rrdname",	CONF_RRDNAME,	NULL			},
	// { "rrdtype",	CONF_RRDTYPE,	NULL			},
	/* End marker */
	{ NULL,		CONF_NULL,	NULL			}
};


/* Global variables, found before the first section and used to
 * initialise any newly created sections.
 *
 * This is mostly initialised to-zero, but values may be set to
 * dynamic string values in the first call to conf_cb().
 */
static struct conf conf_global;


/* Parse a floating point prefix value, or keep the default.
 * Advance the pointer beyond the float, if any.  On failure,
 * return the orignal pointer.
 */
static const char *parse_floatprefix (const char *valstr, double *floatval) {
	const char *endptr = valstr;
	double gotit = strtod (valstr, (char **) &endptr);
	if (endptr != valstr) {
		*floatval = gotit;
	}
	return endptr;
}


/* Parse an unsigned integer prefix value, or keep the default.
 * Advance the pointer beyond the integer, if any.  On failure,
 * return the original pointer.
 */
static const char *parse_intprefix (const char *valstr, unsigned *intval) {
	const char *endptr = valstr;
	unsigned long newval = strtoul (valstr, (char **) &endptr, 0);
	if (endptr != valstr) {
		if (newval != (unsigned) newval) {
			return false;
		}
		*intval = newval;
	}
	return endptr;
}


/* Parse a device address, comma and a (first) register number.
 * Do not change the values when not okay, but then return false.
 */
static bool parse_devreg (const char *valstr, uint8_t *dev, uint16_t *reg) {
//TODO//SPLIT:input:output:AND:USE:dev:reg
	unsigned newdev = 1000;
	unsigned newreg = 1000;
	valstr = parse_intprefix (valstr, &newdev);
	if (*valstr++ != ',') {
		return false;
	}
	valstr = parse_intprefix (valstr, &newreg);
	if (*valstr != '\0') {
		return false;
	}
	if ((newdev > 0x7f) || ((newreg / 10000) == 2 ) || ((newreg / 10000) > 4) || ((newreg % 10000) > 9999)) {
		return false;
	}
	*dev = newdev;
	*reg = newreg;
	return true;
}


/* Parse the offset value.
 */
static bool confparse_offset (struct conf *confio, const char *valstr) {
	double newval = confio->offset;
	const char *endstr = parse_floatprefix (valstr, &newval);
	if (*endstr != '\0') {
		return false;
	}
	confio->offset = newval;
	return true;
}


/* Parse the scale value.
 */
static bool confparse_scale (struct conf *confio, const char *valstr) {
	double newval = confio->scale;
	const char *endstr = parse_floatprefix (valstr, &newval);
	if (*endstr != '\0') {
		return false;
	}
	confio->scale = newval;
	return true;
}


/* Parse a value range, defaulting to -INF..+INF.
 */
static bool confparse_range (struct conf *confio, const char *valstr) {
	double min = -INFINITY, max = +INFINITY;
	valstr = parse_floatprefix (valstr, &min);
	if ((valstr [0] != '.') || (valstr [1] != '.')) {
		return false;
	}
	valstr += 2;
	valstr = parse_floatprefix (valstr, &max);
	if (*valstr != '\0') {
		return false;
	}
	confio->min = min;
	confio->max = max;
	return true;
}


/* Parse a "regtype" value string, and only return true on success.
 */
static bool confparse_regtype (struct conf *confio, const char *valstr) {
	//
	// Collect a fresh datatype value
	int32_t dt = 0;
	//
	// Parse the prefix number for size;
	// the 0 default will to suit each base type
	unsigned size = 0;
	valstr = parse_intprefix (valstr, &size);
	//
	// Parse the base type, modify the size accordingly
	char basetype = *valstr++;
	unsigned bitwidth = 1;
	switch (basetype) {
	case 'i':
		/* Integer type */
		dt = (dt & ~TYPE_BASE_MASK) | TYPE_BASE_INT;
		//
		// Continue into the default, Unsigned
	case 'u':
		/* Unsigned integer (default) + continuation of Integer */
		if (size == 0) {
			size = 16;
		}
		if (size == 16) {
			dt = (dt & ~TYPE_SIZE_MASK) | TYPE_SIZE_16;
		} else if (size == 32) {
			dt = (dt & ~TYPE_SIZE_MASK) | TYPE_SIZE_32;
		} else {
			return false;
		}
		break;
	case 'f':
		/* Floating-point */
		if (size == 0) {
			size = 32;
		}
		if (size == 32) {
			dt = (dt & ~TYPE_SIZE_MASK) | TYPE_SIZE_32;
		} else if (size == 64) {
			dt = (dt & ~TYPE_SIZE_MASK) | TYPE_SIZE_64;
		} else {
			return false;
		}
fprintf (stderr, "Floating point with size %d yields dt & TYPE_SIZE_MASK = 0x%08x for %d registers\n", size, dt & TYPE_SIZE_MASK, 1 + (((dt & TYPE_SIZE_MASK) >> TYPE_SIZE_SHIFT)));
		break;
	case 'b':
		/* Bits or bit fields */
		valstr = parse_intprefix (valstr, &bitwidth);
		if (size + bitwidth > 16) {
			return false;
		}
		dt = (dt & ~TYPE_LOW_MASK & ~TYPE_HIGH_MASK) | (size << TYPE_LOW_SHIFT) | ((size+bitwidth-1) << TYPE_HIGH_SHIFT);
		break;
	default:
		/* Unrecognised base type, treated like 16u */
		;
	}
	//
	// Parse any postfix annotations
	while (*valstr != '\0') {
		unsigned arrsz = 1;
		switch (*valstr++) {
		case '~':
			/* Invert bits */
			//TODO// Only for bit fields
			dt |= TYPE_FLAG_INVERT;
			break;
		case '<':
			/* Little Endian */
			//TODO// Only for integer and float values
			dt |= TYPE_FLAG_LENDIAN;
			break;
		case '>':
			/* Big Endian */
			//TODO// Only for integer and float values
			if ((dt & TYPE_FLAG_LENDIAN) != 0) {
				return false;
			}
			break;
		case '*':
			/* Time-integrated Flow */
			dt |= TYPE_FLAG_FLOW;
			break;
		case '#':
			/* Array postfix */
			if (arrsz != 1) {
				return false;
			}
			valstr = parse_intprefix (valstr, &arrsz);
			if ((arrsz == 0) || (arrsz > TYPE_ARRAY_MASK)) {
				return false;
			}
fprintf (stderr, "Array size is %d\n", arrsz);
			dt = (dt & TYPE_ARRAY_MASK) | arrsz;
			break;
		default:
			/* Unrecognised character */
			return false;
		}
	}
	//
	// The whole string was parsed successfully
	confio->datatype = dt;
	return true;
}


/* Parse an "input" value string as device and register pair.
 */
static bool confparse_input (struct conf *confio, const char *valstr) {
	return parse_devreg (valstr, &confio->input_dev, &confio->input_reg);
}


/* Parse an "output" value string as device and register pair.
 */
static bool confparse_output (struct conf *confio, const char *valstr) {
	return parse_devreg (valstr, &confio->output_dev, &confio->output_reg);
}


/* Callback from the INI parser.  The user is a pointer to the list head
 * of a configuration structure.
 */
static int conf_cb (void *user, const char *section, const char *name, const char *value) {
	//
	// Retype the user pointer to the list-head pointer
	struct conf **accu = (struct conf **) user;
	assert (*accu != NULL);
	//
	// Reject invalid input
	if ((name == NULL) || (value == NULL)) {
		return 1;
	}
fprintf (stderr, "[%s].%s = %s\n", section, name, value);
	//
	// Find the index for the variable name
	int i = 0;
	while (strcasecmp (confwords [i].name, name) != 0) {
		i++;
		if (i >= CONF_NUMVARS) {
			/* Not found */
			return 1;
		}
	}
	enum confvar index = confwords [i].index;
	//
	// Check if the section is the same as before
	// This initially matches the global variables
	if (strcasecmp ((*accu)->var [CONF_SECTION], section) != 0) {
		//
		// Create a new object
		struct conf *new = calloc (1, sizeof (struct conf));
		if (new == NULL) {
			return 1;
		}
		//
		// Find the last *accu element
		struct conf *globals = *accu;
		if (globals != NULL) {
			while (globals->previous != NULL) {
				globals = globals->previous;
			}
		}
		//
		// Copy the globals as duplicate strings
		for (int i = 0; i < CONF_NUMVARS; i++) {
			if (i == CONF_SECTION) {
				continue;
			}
			if (globals->var [i] != NULL) {
				new->var [i] = strdup (globals->var [i]);
				if (new->var [i] == NULL) {
					return 1;
				}
			}
		}
		//
		// Now set the section name
		new->var [CONF_SECTION] = strdup (section);
		//
		// Copy the derived values, although they are unlikely to be set
		new->datatype = globals->datatype;
		new->offset   = globals->offset;
		new->scale    = globals->scale;
		new->min      = globals->min;
		new->max      = globals->max;
		//
		// Setup the new configuration section
		new->previous = *accu;
		*accu = new;
	}
	//
	// Test with the detailparser, if we have any
	if (confwords [i].detailparser != NULL) {
		if (!confwords [i].detailparser (*accu, value)) {
			return 1;
		}
	}
	//
	// Cleanup the current value, if any
	if ((*accu)->var [index] != NULL) {
		free ((void *) (*accu)->var [index]);
	}
	//
	// Setup the variable with a duplicate of the value
	(*accu)->var [index] = strdup (value);
	//
	// Return success or failure
	return ((*accu)->var [index] != NULL) ? 0 : 1;
}


/* Read the configuration from the named file.
 * Cleanup later with conf_free().
 * Return NULL for failure.
 */
struct conf *conf_read (const char *filename) {
	//
	// Collect the return value in a data pointer
	struct conf *retval = calloc (1, sizeof (struct conf));
	if (retval == NULL) {
		return retval;
	}
	//
	// Setup a few gentle starting points
	retval->var [CONF_SECTION] = strdup ("");
	retval->var [CONF_SERVER]  = strdup ("::1");
	retval->var [CONF_PORT]    = strdup ("502");
	retval->var [CONF_RANGE]   = strdup ("..");
	retval->datatype = 0;
	retval->offset = 0.0;
	retval->scale  = 1.0;
	retval->min = -INFINITY;
	retval->max = +INFINITY;
	//
	// Parse the configuration file with INI
	if (ini_parse (filename, conf_cb, &retval) == -1) {
		return NULL;
	}
	//
	// Return the collected configuration
	return retval;
}


/* Cleanup a configuration loaded with conf_read().
 * This function should never fail.
 */
void conf_free (struct conf *conf) {
	//
	// Iterate over all configuration elements
	while (conf != NULL) {
		struct conf *prev = conf->previous;
		for (int i = 0; i < CONF_NUMVARS; i++) {
			if (conf->var [i] != NULL) {
				free ((char *) conf->var [i]);
			}
		}
		free (conf);
		conf = prev;
	}
}


/* Create a cmdio structure from a configuration element
 * and add it to the event loop for the bus.
 */
bool conf_cmdio_add (struct conf *conf) {
	//
	// Allocate the cmdio structure
	struct cmdio *cmdio = calloc (1, sizeof (struct cmdio));
	if (cmdio == NULL) {
		goto fail;
	}
	//
	// Setup the cmdio structure
	unsigned arrlen = (conf->datatype & TYPE_ARRAY_MASK);
	if (arrlen == 0) {
		arrlen = 1;
	}
	unsigned numregs, reqlen, anslen;
	uint8_t dev = conf->input_dev;
	uint16_t reg = conf->input_reg;
	switch (conf->datatype & TYPE_BASE_MASK) {
	case TYPE_BASE_UINT:
	case TYPE_BASE_INT:
	case TYPE_BASE_FLOAT:
	case TYPE_BASE_FIELD:
		numregs = (1 + ((conf->datatype & TYPE_SIZE_MASK) >> TYPE_SIZE_SHIFT)) * arrlen;
fprintf (stderr, "size = %x, dt&MASK = %x, dt = 0x%08x, arrlen = %d, numregs = %d\n", 1+((conf->datatype & TYPE_SIZE_MASK) >> TYPE_SIZE_SHIFT), (conf->datatype & TYPE_SIZE_MASK), conf->datatype, arrlen, numregs);
		if (numregs <= 1) {
fprintf (stderr, "Command 4/16, #req=3, #rsp=2\n");
			// Mostly the same as for: numregs > 1
			ALLOC_CMDIO (cmdio, fail, dev, 4 /*inpute_regs:BUT:TODO:read_holding_regs==3*/, 4, 1 + 2);
		} else {
fprintf (stderr, "Command 4/6, #req=4, #rsp=%d\n", 1+2*numregs);
			// Mostly the same as for: numregs <= 1
			ALLOC_CMDIO (cmdio, fail, dev, 4 /*input_reg:BUT:TODO:read_holding_reg==6*/, 4, 1 + 2 * numregs);
		}
		cmdio->data [0] = reg >> 8;
		cmdio->data [1] = reg & 0xff;
		cmdio->data [2] = numregs >> 8;
		cmdio->data [3] = numregs & 0xff;
		break;
	case TYPE_BASE_BIT:
		anslen = 1 + (arrlen + 7) / 8;
fprintf (stderr, "Command 2, #req=2, #rsp=%d\n", anslen);
		ALLOC_CMDIO (cmdio, fail, dev, 2 /*discrete_inputs:BUT:TODO:read_coils==1*/, 2, anslen);
		break;
	default:
		goto free_fail;
	}
	//
	// Register in the event loop of the bus
	//TODO// Timing is allocated a bit erratically, but should be user-defined
	static unsigned disperser = 0;
	bool added = cmdio_add (cmdio, cmdio_version, disperser++, 60);
	if (!added) {
		goto free_fail;
	}
	//
	// Return success
	return true;
	//
	// Report failure, possibly after cleaning up
free_fail:
	free (cmdio);
fail:
	return false;
}
