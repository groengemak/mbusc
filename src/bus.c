/* Modbus TCP or RTU interface for event-driving polling.
 *
 * The cmdio_add() and cmdio_del() can be used to add cmdio structures,
 * to setup commands that are sent at a regular beat.  Run cmdio_loop()
 * to actually start this procedure.  Each cmdio structure defines a
 * response callback hook (respcb) that allows the processing of data.
 *
 * The structures below consider address and data, but the data[] is
 * an abstract byte array to be setup and parsed in the callback hooks.
 * The CRC for Modbus RTU mode is concealed, as is the TCP header for
 * Modbus TCP.  Request and response length should be set to the length
 * of the data[] byte array.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <math.h>
#include <string.h>

#include <unistd.h>
#include <errno.h>

#include <time.h>

#include <arpa2/except.h>

#include "bus.h"



/* The modbus_rtu flag is set when commands use Modbus RTU.  If it is
 * reset, then modbus_tcp is used.  These may not be options in all
 * compiled programs.  When both are available, Modbus TCP is default.
 */
#undef SUPPORT_MODBUS_CHOICE
#ifdef SUPPORT_MODBUS_RTU
#  ifdef SUPPORT_MODBUS_TCP
#    define SUPPORT_MODBUS_CHOICE
static bool modbus_rtu = false;
#  else
static const bool modbus_rtu = true;
#  endif
#else
static const bool modbus_rtu = false;
#endif


/* The modbus file descriptor, facilitating the modbus_rtu variant.
 * This value is only meaningful while executing the modbus_loop().
 */
static int modbus = -1;


/* The response queue is filled with cmdio values waiting for a response.
 * The index in the queue is the low byte of the cyclic txnid counter.
 * This allows up to 256 requests in transit at any time.
 *
 * Queue elements are set to NULL upon delivery, to avoid responding more
 * than once to a request.  Resends also reset the queue entry (if matched)
 * before trying again with a fresh txnid.
 */
static struct cmdio *response_queue [256];

/* When using Modbus RTU instead of Modbus TCP, the response_queue is just
 * a round-robin of the scheduled requests.  New requests are stored at
 * response_todo++ and when response_done differs, it handles each of the
 * values followed by response_done++.
 */
static uint8_t response_todo = 0;
static uint8_t response_done = 0;


/* The next transaction identity to send.
 */
static unsigned txnid_next;


/* The errno value to return from cmdio_run().
 */
static int ev_errno = 0;


/* Error codes 1, 2, ... in array indexes 0, 1, ... BUS_ERROR_CODES-1
 */
#define BUS_ERROR_CODES 11
static const char *bus_error_string [] = {
	"Illegal Function",
	"Illegal Data Address",
	"Illegal Data Value",
	"Server Device Failure",
	"Acknowledge",
	"Server Device Busy",
	"Negative Acknowledge",
	"Memory Parity Error",
	"Non-Standard Nine",
	"Gateway Path Unavailable",
	"Gateway Target Device Failed to Respond"
};


/* Receive a series of bytes from the Modbus file descriptor.
 * Build up towards a full response, which is then replied.
 */
static void _recv_cmdio_event (EV_P_ ev_io *io, int revents) {
	//
	// The static buffer space for holding the I/O frame
	static uint8_t buf [256];
	static uint16_t gotlen = 0;
	static uint16_t getlen = 0;
	static const uint16_t maxlen = sizeof (buf);
	//
	// Read the bytes that we can get
	//TODO// Assumes partial data if all is gone
	ssize_t newlen = read (modbus, buf + gotlen, getlen ? getlen : maxlen);
log_debug ("read #%d bytes\n", (int) newlen);
	if (newlen <= 0) {
		if (newlen == 0) {
			ev_errno = ECONNRESET;
			goto breakloop;
		} else if ((errno != EAGAIN) && (errno != EWOULDBLOCK)) {
			ev_errno = errno;
			goto breakloop;
		}
		return;
	}
	gotlen += newlen;
	//
	// Possibly learn a receiving length; return if too early
moredata:
	if (getlen == 0) {
		if (modbus_rtu) {
#ifdef SUPPORT_MODBUS_RTU
			if (gotlen < 2) {
				return;
			}
			getlen = ... + 2;
#endif
		} else {
			if (gotlen < 6) {
				return;
			}
			getlen = 6 + (buf [4] << 8) + buf [5];
		}
	}
	//
	// Possibly log the received data for this one frame
	log_data ("Modbus TCP recv", buf, getlen, 0);
	//
	// Return if we have not gotten everything yet
	bool goterr = (buf [modbus_rtu ? 1 : 6 + 1] & 0x80) == 0x80;
	if (goterr && modbus_rtu) {
		getlen = modbus_rtu ? 3 : 6 + 3;
	}
	if (gotlen < getlen) {
		return;
	}
	//
	// Deliver the frame to the cmdio request
	if (modbus_rtu) {
#ifdef SUPPORT_MODBUS_RTU
		//
		// Modbus RTU data is followed by a checksum
		//TODO// Check the checksum
		;
		//
		// Check for error codes
		//
		;
		//TODO// Check address, command, length...
		...cmdio->respcb (...cmdio, cmdio->_poll_timer.interval, buf);
#endif
	} else {
		//
		// Check for error codes
		if (goterr) {
			uint8_t errcode = buf [6 + 2];
			const char *errstr = "Non-Standard";
			if (errcode - 1 < BUS_ERROR_CODES) {
				errstr = bus_error_string [errcode - 1];
			}
			log_error ("Address 0x%02x, Command 0x%02x, Error 0x%02x (%s)",
					buf [6 + 0], buf [6 + 1], buf [6 + 2],
					errstr);
			goto skipmsg;
		}
		//
		// Modbus TCP data is preceded by a header
		// that starts with txnid_h, txnid_l
		struct cmdio *cmdio = response_queue [buf [1]];
		if (cmdio == NULL) {
			goto skipmsg;
		}
		if (buf [0] != cmdio->txnid_h) {
			goto skipmsg;
		}
		//
		// Call the response callback
		cmdio->respcb (cmdio, cmdio->_poll_timer.interval, buf + 6 + 2);
		//
		// Remove the command from the response queue
		response_queue [buf [1]] = NULL;
	}
skipmsg:
	//
	// If more data is presented, cycle it to the start
	if (gotlen > getlen) {
		memcpy (buf, buf + getlen, gotlen - getlen);
	}
	gotlen -= getlen;
	getlen = 0;
	if (gotlen > 0) {
		goto moredata;
	}
	//
	// Done, handled all, quiety leave this handler
	return;
	//
	// Break out of the event loop
breakloop:
	ev_break (EV_DEFAULT_ EVBREAK_ONE);
	return;
}


/* Send a cmdio structure as part of a continuous loop.  Exactly one
 * callback is made per period, normally with proper RRD timing, but
 * when the device does not response or other things go wrong there
 * will be no callback at all.
 *
 * TODO: May have to split this to accommodate resending
 */
static void _send_cmdio_periodic (EV_P_ ev_periodic *tmr, int revents) {
	//
	// Map the timer data structure to the containing cmdio
	struct cmdio *cmdio = (struct cmdio *) tmr;
	//
	// Send the command out to Modbus TCP or Modbus RTU
	if (modbus_rtu) {
		//
		// Only proceed if the queue is not full
		uint8_t work = response_todo - response_done;
		if (work == 255) {
			return;
		}
		//
		// Add our command to the queue
		response_queue [response_todo++] = cmdio;
		//
		// If this is a first, start sending
		if (work == 0) {
			//TODO// START THE RTU SENDING PROCEDURE
		}
		;
	} else {
		//
		// Remove any old occurence in the response_queue
		if (response_queue [cmdio->txnid_l] == cmdio) {
			response_queue [cmdio->txnid_l] = NULL;
		}
		//
		// Set a fresh txnid value
		cmdio->txnid_h = (txnid_next >> 8) & 0xff;
		cmdio->txnid_l =  txnid_next       & 0xff;
		//
		// Enqueue the transaction
		response_queue [cmdio->txnid_l] = cmdio;
		//
		// Possibly log the received data for this one frame
		log_data ("Modbus TCP send", &cmdio->txnid_h, 6 + 256*cmdio->reqlen_h + cmdio->reqlen_l, 0);
		//
		// Atomically send to Modbus TCP
		// Ignore failures; we need to handle them for later trouble too
		write (modbus, &cmdio->txnid_h, 6 + 256*cmdio->reqlen_h + cmdio->reqlen_l);
	}
	//
	// Increment the txnid to indicate we sent something
	txnid_next++;
}


/* Add a cmdio structure to the polling loop, to be used forevermore.
 * The version to interpret is provided by the calling module, and should
 * be set to cmdio_version.
 *
 * The start is the time at which the RRD polling started, and it indicates
 * a good time to start polling (modulo the period).  The next offset will
 * be determined, and the period then continues a regular poll.  This aligns
 * as well as possible with the RRD timing.
 *
 * Return true on success.  On failure, set errno and return false.
 */
bool cmdio_add (struct cmdio *addend, cmdio_version_t interp, time_t start, double period) {
	//
	// Wrong versions return failure
	if (interp != cmdio_version) {
		errno = ENOSYS;
		return false;
	}
	//
	// Restrict the response buffer length to ADU maxlen 256
	if (addend->resplen > 256) {
		errno = ENOMEM;
		return false;
	}
	//
	// Compute the CRC when sending over Modbus RTU
	//TODO// Cannot know this yet, this knowledge moved to cmdio_loop()
#if 0
	if (modbus_rtu) {
		//TODO// Implement CRC computation
		return false;
	}
#endif
	//
	// Setup the next event timout
	ev_tstamp offset = fmod (start, period);
	ev_periodic_init (&addend->_poll_timer, _send_cmdio_periodic, offset, (ev_tstamp) period, 0);
	ev_periodic_start (EV_DEFAULT_ &addend->_poll_timer);
	//
	// Return success
	return true;
}


/* Remove a cmdio structure from the polling loop.
 */
bool cmdio_del (struct cmdio *goner, cmdio_version_t interp) {
	//
	// Wrong versions return failure
	if (interp != cmdio_version) {
		errno = ENOSYS;
		return false;
	}
	//
	// Stop the event timer
	ev_periodic_stop (EV_DEFAULT_ &goner->_poll_timer);
	//
	// Return success
	return true;
}


/* Run a Modbus loop over the given file descriptor; with rtu set,
 * this is considered a serial bus for Modbus RTU, otherwise it is
 * considered a socket for Modbus TCP.
 *
 * This function may run indefinitely, but it will return when an
 * unrecoverable error occurs, like memory errors or communication
 * problems.  EAGAIN/EWOULDBLOCK do not count as unrecoverable.
 *
 * Returns true if *something* was sent, false otherwise.
 * Always sents errno, check this before looping around.
 *
 * It is possible to call this again with the same or new Modbus
 * coordinates.  Everything setup with cmdio_add() is still in
 * the event loop, and anything queued still is.  The work can
 * just continue; this is just what you want for Modbus RTU, and
 * it is not usually damaging for Modbus TCP.
 */
bool cmdio_loop (int fd, bool rtu) {
	//
	// Check the file descriptor
	if (fd < 0) {
		errno = ENODEV;
		return false;
	}
	//
	// Use the rtu setting if it is acceptable
	if (rtu != modbus_rtu) {
		#ifdef SUPPORT_MODBUS_CHOICE
			modbus_rtu = rtu;
		#else
			errno = ENOSYS;
			return false;
		#endif
	}
	//
	// Set the modbus file descriptor
	modbus = fd;
	//
	// Enable the Modbus watcher
	ev_io modbus_io;
	ev_io_init (&modbus_io, _recv_cmdio_event, fd, EV_READ);
	ev_io_start (EV_DEFAULT_ &modbus_io);
	//
	// Run the event loop
	unsigned txnid_orig = txnid_next;
	ev_run (EV_DEFAULT_ 0);
	//
	// Disable the Modbus watcher
	ev_io_stop (EV_DEFAULT_ &modbus_io);
	//
	// Retract the modbus file descriptor
	modbus = -1;
	//
	// Set the loop's evno and return
	errno = ev_errno;
	return (txnid_next != txnid_orig);
}


