/* conf.h -- Configuration data strings (with a few extras)
 *
 * Configuration data is stored in an array of strings.  The index
 * of each setting is derived from an enum.  The conf.c fills these
 * structures from an INI file and it also provides parsers to
 * further map the strings to internal data.
 *
 * The configuration mechanism involves a mechanism for defaults,
 * which may be set for all the sections as var=value lines before
 * the first section.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdint.h>
#include <math.h>



/* Enumeration of recognised configuration variables.
 */
enum confvar {
	CONF_SECTION,		/* Section name */
	CONF_NAME,		/* Name for this variable, replaces section name */
	CONF_DESCR,		/* Description for human output */
	CONF_UNIT,		/* Postfix unit name (after offset and scale) */
	CONF_OFFSET,		/* Offset to raw measurement */
	CONF_SCALE,		/* Scale for raw measurement + offset */
	CONF_RANGE,		/* [min]..[max] range in units */
	CONF_SERVER,		/* Server address for Modbus TCP, default ::1 */
	CONF_PORT,		/* Server port    for Modbus TCP, default 502 */
	CONF_REGTYPE,		/* How to read the register(s) from Modbus, default "16u" */
	CONF_INPUT,		/* This is an  input register (among others) */
	CONF_OUTPUT,		/* This is an output register (among others) */
	// CONF_COMPUTE,		/* TODO */
	// CONF_GRAPH,		/* TODO */
	// CONF_EXPORT,		/* TODO */
	CONF_RRDFILE,		/* Filename for the RRD to store this measurement */
	CONF_RRDNAME,		/* Name for this variable in the RRD, replaces name */
	// CONF_RRDTYPE,		/* Type of RRD entry.  TODO: Read with rrdinfo */
	/* Closing counter */
	CONF_NUMVARS,		/* The number of variables in an array */
	CONF_NULL = -1		/* No name, not the default after clearing memory */
};



/* Declaration of configuration data; both string forms and derived fields.
 */
struct conf {
	//
	// Configurations form a linked list; the last is section "" for defaults
	struct conf *previous;
	//
	// The strings as read from the configuration file
	const char *var [CONF_NUMVARS];
	//
	// The value for the datatype
	uint32_t datatype;
	//
	// The floating-point values for offset and scale
	double offset, scale;
	//
	// The valid range, with infinity for no constraint
	double min, max;
	//
	// Device number and register: input, output
	uint16_t input_reg, output_reg;
	uint8_t  input_dev, output_dev;
};


/* Read the configuration from the named file.
 * Cleanup later with conf_free().
 * Return NULL for failure.
 */
struct conf *conf_read (const char *filename);


/* Cleanup a configuration loaded with conf_read().
 * This function should never fail.
 */
void conf_free (struct conf *conf);


/* Create a cmdio structure from a configuration element
 * and add it to the event loop for the bus.
 */
bool conf_cmdio_add (struct conf *conf);

