# Modbus Client|Collector|Computation|Controller

> *This is a data collection client for Modbus TCP.  Data is stored
> in cyclic RRD buffers; it may learn derived values and drive
> output from it.*

The name `mbusc` mirrors that of
[`mbusd`](https://github.com/3cky/mbusd)
which maps Modbus TCP to Modbus RTU, while it takes care
of request multiplexing, checksumming and other transport
issues.

## Roles of mbusc

There are a few things that `mbusc` can do.


### Collection of Data

One task of `mbusc` is to regularly poll data sources and to store
the output in
[RRD lists](https://oss.oetiker.ch/rrdtool/doc/rrdtool.en.html),
which form pre-allocated cyclic structures for sampled data.
Such sample series may loose detail as they age.  Tools exist to do
most imaginable tasks with this data.

Before storing the data, `mbusc` transforms a measurement to a
standardised unit, which may involve scaling and offsetting.
The unit and measurement accuracy can also be configured.


### Control of Outputs

Another task of `mbusc` is to drive outputs.  These can be
imaged as having a standardised unit, but an output to drive is
determined such that the same mapping of scaling and offsetting
on the driven value maps back as well as possible to the
unit-based value as closely as possible.

TODO:
When a setting is explicitly updated, it constitutes a new
requirement for the system.  This requirement is leant for
the time and for the day of the week as the temperature that
should have been set.  The learning process is gentle, and
not absolute, so you need to make settings a few times before
it is adopted in the scheme.  Active changes to the normal
behaviour are considered to indicate discomfort.  Old settings
are unlearnt or softened if they could lead to jumpy control.

Every input may also be configured as an output.  An example
where this is indeed the case is when a thermostat can be read
while it actively drives a heating or cooling device.  The fact
that it is considered an output as well means that `mbusc` is
mindful of the possibility that an input can have an effect on
an output.


### Computation of Feedback

When outputs are controlled, they are assumed to have an impact
on inputs.  Or not.  In general, `mbusc` tries to learn the
step response function for each of its controlled outputs,
assuming a linear system in terms of the units.

Note that a step response is predictive, and may well involve
integration over time.  An example is a heating element which
adds heat for as long as it is turned on.

The purpose of a computation is to make one or more values
equal to a setting in a nearby future.  The step function
indicates how the current situation will project, and whether
it will be enough, or perhaps that more or less driving is
needed.

As a special form of driving, it is possible to set a goal
at a future point in time.  An example is a particular room
temperature in the morning.  When the system considers the
various environmental factors, it may decide to switch on
the heating just in time to have the desired temperature at
the given time.


## Fun Example

Let's say that you set a room temperature of 18 ⁰C at 7:00 in
the morning.  And let's say that you measure the current room
and outside temperature.

The outside pulls out heat at a pace that depends on the level
of insulation, and for which a step resposne function has been
learnt.  And a heating element adds heat at another pace, as
learnt by another step response function.  The predictions of
the step response functions can now be solved to decide when
the explicit heater should be switched on.

Let's extend this a bit.  You have a compost pile that, in some
ingeneous manner, helps to heat your home.  This is a system
that can be controlled with moist level, ventilation and the
C:N ratio, but it will respond very slowly.  In fact, it will
change its step function to indicate that.  Measurements help
to learn the constant heat produced, and a step function has
been learnt about the influence of that heat on room temperature.

This compost pile is just another source of heat, just like
the outside temperature is a drain of heat.  The sum of the
influences needs to match 18 ⁰C at 7:00 and if the compost
produces more heat it will allow the later switching-on of
the explicit heating system.

Though it may seem silly to heat with compost, this has been
done in various concrete settings.  It works well, but is not
a reliable or fast-responding system.  It can however be very
useful as an opportunistic source of heat that reduces the
dependency of the systems that are reliable and fast to
respond.


## Configuration Files

The behaviour of `mbusc` is determined by a configuration
file, which defaults to `/etc/mbusc/mbusc.conf` if no other
value is provided on the commandline.

Configuration files are INI styled; sections are logical
names for easy reference and for logging, with the following
settings:

  * `rrdfile` is the filename of the RRD that stores
    values.  The file must have been created before
    any data can be written.  A polling frequency for
    the value is extracted from the RRD setup.

    The filename is either an absolute path starting
    with a slash, or it is relative to `/var/lib/mbusc/`
    if writeable, otherwise relative to `~/.cache/mbusc/`
    which is created when absent.

    This value may be set before any sections and serve
    as a default.

  * `regtype` is an indication of the variable type
    and its size, like `32i` or `16u` for signed and
    unsigned integers, in 32 and 16 bits, respectively.

    The types `32f` and `64f` are for IEEE
    floating-point numbers.

    The type `b` is for 0 or 1; use `b~` to invert the
    bit (or bits), independently done before applying
    the offset and scale.  Only the bits of the field
    (so default only one bit) is inverted.

    Prefix a size for a bit field, as in `4b` for a
    nibble, but do not follow it with `~`.  Optionally
    follow with a shift to higher bits, like `4b4` for
    the second-lowest nibble.  For the current version,
    the sum of the prefix and postfix number must not
    exceed 16, the size of a Modbus register.

    The bit can be used as a number or a flag, as
    desired.  Bit fields are always numbers.
    Note that this type gives access to the
    [coil input](https://ozeki.hu/p_5876-mobdbus-function-code-1-read-coils.html),
    [single coil output](https://ozeki.hu/p_5880-mobdbus-function-code-5-write-single-coil.html),
    [multiple coil output](https://ozeki.hu/p_5882-mobdbus-function-code-15-write-multiple-coils.html)
    and
    [multiple discrete input](https://ozeki.hu/p_5877-mobdbus-function-code-2-read-discrete-inputs.html)
    Modbus commands.

    The default type is `u16`.

    Modbus TCP normally uses
    [big endian](https://www.rtautomation.com/technologies/modbus-tcpip/)
    byte order, but it is possible to
    [append `<`](https://wingpath.co.uk/modbus/modbus_extensions.php)
    to reverse the order of the registers; but note that the
    2 bytes in a 16-bit register are still sent in big-endian.
    Append `>` for big endian, but this is also the default.

    Postfix `*` after a type to make it multiplied by the sample
    period in seconds and then summed continuously;
    these are so-called "flow measurements", and the version
    multiplied by time will indicate how the flowing value
    piles up.  An example is power that piles up to energy.
    TODO: Should this be in the `rrdtype` or `regtype`?

    Postfix `#n` to repeat the last value `n` times, and append
    `[0]` to `[n-1]` to the `name` for component names.
    Every new value will start with the register following the
    last one used for the basic value size.
    It is valid to not name the type so the default type can
    be used.  This is one form to sample multiple values in
    one request, even when they are not stored in the RRD.

    Append `@x` to reference a previously defined value named `x`.
    The value `x` determines sample timing and triggers its references.
    This must encompass the larger value, but intstructs loading
    the current name as part of a larger whole.  A likely use
    is to reference a `#n` that is not stored by itself, and take
    its components apart.  Another would be to isolate bit fields
    from a register reading.

  * `name` is a variable name for the measurement in
    this section.  It overrides the section name, which
    may be useful for user communication while this
    `name` is useful as an internal identity.

  * `rrdname` is a variable name for the RRD data source
    for this section.  It overrides the `name` or, if that
    is not set, the section name.

  * `rrdtype` is the RRD type name, default `GAUGE` for
    simple samples of the current situation.  Use `NULL`
    to explicitly not store the value.  Note that `Dxxx`
    types may be used to compensate for a `*` postfixed
    readout (integrated over time, then differentiated)
    which may have a number of purposes.

  * `descr` can be used in informal reports, such as an
    HTML page with data, and may be any meaningful
    string; an example would be `Kitchen Temperature`.
    It is also helpful when reading the configuration.

  * `unit` is a postfix string to be printed after the
    register value after passing it through `offset`
    and `scale` transformations.

  * `offset` and `scale` are added to and multiplied
    with (in that order) the register values to map
    them to a variable with the given `unit` and
    optional `name`.

    To remember the order: Find the specification for
    the 0 as represented in the register, and subtract
    that (add its negative value).  Then, scale to the
    desired `unit`.  So, the `offset` is in terms of the
    Modbus register value while the `scale` is the number of
    `unit` increments per 1 or 1.0 register value increment.

  * `range` can be used to set an optional minimum and an
    optional maximum value in `unit` terms, so after
    application of `offset` and `scale`.  The syntax is
    `min..max` or `..max` or `min..` or `..` where absent
    values set the most permissive value possible.

  * `server` sets the Modbus TCP server address, as an
    IPv4 or IPv6 address.  It may be set as a global
    option, which then becomes the default for later
    sections.  If both are missing, the value used
    is `::1` for localhost.

  * `port` sets the Modbus TCP port to a decimal value.
    It may be set as a global option, which then becomes
    the default for later sections.  If both are missing,
    the value used is the standard default `502`.  Note
    that this is a protected port on any modern operating
    system.

    The use of alternate `server` and `port` values allows
    the combination of more than one Modbus TCP or RTU
    sources.

  * `input` indicates a device address and input register
    separated by a comma.

    Example: `0x3a,30` with `regtype u16` is the input register starting
    at address 30 (decimal) of the device at address 0x3a (hexadecimal).

    An `input` value may also be an `output` value, in
    the same or another register.  It may not also be a
    `compute` value.

  * `output` indicates a device address and output register
    separated by a comma.

    Example: `123,1` with `regtype b` is the coil at address 1
    of the device at address 123 (decimal).

    An `output` value may also be an `input` value, in
    the same or another register.  Alternatively, an
    `output` value may also be a `compute` value.

  * `compute` derives a value based on other parts in the same
    RRD.  It is an RPN expression for RRD which results in a
    value in `unit`.

    Use `compute` as an alternative to `input`.  It may be
    combined with `output` but not with `input`.

  * `graph` defines a value to be drawn in a graphic, if so
    requested.  (TODO)

  * `export` defines a value to be exported in an export file,
    if so requested.  (TODO)

**TODO:**

  * Config: Polling frequency and cycles of interest.
    (Sane default? 10y@1D, year@1H, [month@1H,] week@15M, day@1M cycles.)
    *No.  Pull this from the RRD using `rrdinfo`.*

  * Commandline:  Generate an RRD template file for `rrdcreate` and `rrdtune`.
    *No.  RRD holds much more data.*  We'd rather rely on `rrdinfo` for meta.

  * Config or Commandline: `rrdcache` to link to a RRD cache daemon.
    *No.  Handled by `rrdupdate` with an envvar.*

  * Device polling (for on/off state) may be intensified in a close range, but how?
    Is this meaningful for all devices?  Hmm, maybe for `*` flows (time-integrated sensors).

  * When to write data to an RRD?  Next period perhaps?  Or after a timeout!
    *Done.*  It is collected per RRD and writes can be grouped in an `xargs` managed loop.

