/** @defgroup arpa2except Exception and Error Handling
 *
 * The purpose of <arpa2/except.h> is to provide with lavish functionality
 * for providing user feedback.  Easy macro definitions allow the output
 * to be redirected or suppressed.  As a result, you can be highly verbose
 * without necessarily weighing on runtime performance.  There is even an
 * extra symbol DEBUG_DETAIL which allows extra macros log_detail() to dump
 * specifics beyond even the debugging level, and log_data() to make dumps
 * of data packets (such as might travel over a network).
 *
 * If you include this file, your CMakeLists should include(ExceptionHandling)
 *
 * Configuration option: LOG_STYLE may be set to one of:
 *  - LOG_STYLE_NULL    -- log information will be suppressed
 *  - LOG_STYLE_STDOUT  -- log information is printed to stdout
 *  - LOG_STYLE_STDERR  -- log information is printed to stderr
 *  - LOG_STYLE_SYSLOG  -- log information
 *  - LOG_STYLE_DEFAULT -- platform-sensible default choice from the above
 * 
 * @{
 */

/* This file is part of ARPA2 Common Libraries, version 2.0.2 and later.
 *     https://gitlab.com/arpa2/arpa2common
 *
 * Before, it was kept in ARPA2CM, where it was taken from commit
 *     6c995b1a28190c1c5f477edca642999ebf45e243
 *     https://gitlab.com/arpa2/arpa2cm
 *
 * Redistribution and use is allowed according to the terms of the two-clause BSD license.
 *    SPDX-License-Identifier: BSD-2-Clause
 *    SPDX-FileCopyrightText: Copyright 2020, Rick van Rein <rick@openfortress.nl>
 *
 */


#ifndef _ARPA2_EXCEPT_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __clang__
/* This file uses ## __VA_ARGS__ all the time, which is a GNU extension;
 * tell Clang not to complain about it for this one header.
 */
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu-zero-variadic-macro-arguments"
#endif

#ifdef DEBUG

/** @brief except_if(lab,nok)
 *
 * Test a condition and take an exceptional course on nok.
 *
 * The exceptional course is to log an error including the condition and
 * file/line information, then jump to the label lab (presumably for
 * cleanup).  Though simple, this mimics a reasonable part of exception
 * handling as it is known in more advanced languages.  If a value in
 * errno is needed, this is assumed to already be present.
 *
 * These are the versions used when DEBUG is defined.
 */
#define except_if(lab,nok)   do if (nok)   { log_error ("Exceptional outcome for %s at %s:%d", #nok, __FILE__, __LINE__); goto lab; } while (0)

/** @brief except_ifnot(lab,ok)
 *
 * Test a condition and take an exceptional course on !ok.
 *
 * The exceptional course is to log an error including the condition and
 * file/line information, then jump to the label lab (presumably for
 * cleanup).  Though simple, this mimics a reasonable part of exception
 * handling as it is known in more advanced languages.  If a value in
 * errno is needed, this is assumed to already be present.
 *
 * These are the versions used when DEBUG is defined.
 */
#define except_ifnot(lab,ok) do if (!(ok)) { log_error ("Exceptional outcome for %s at %s:%d", #ok , __FILE__, __LINE__); goto lab; } while (0)

/** @brief assertxt(test,fmt,...)
 *
 * Ascertain that test is true; otherwise, log a message with the test, file/line
 * information and a formatted string before exiting just like assert() would.
 *
 * This is the versions used when DEBUG is defined.
 */
#define assertxt(test,fmt,...) do if (!(test)) { log_critical ("\nAssertion Failed on %s at %s:%d (" fmt ")\n", #test, __FILE__, __LINE__ , ##__VA_ARGS__); exit (-6); } while (0)

#else /* DEBUG */

/** @brief except_if(lab,nok)
 *
 * With DEBUG undefined, simply jump to lab on nok.
 */
#define except_if(lab,nok)   while ( nok ) goto lab

/** @brief except_ifnot(lab,ok)
 *
 * With DEBUG undefined, simply jump to lab on !ok.
 */
#define except_ifnot(lab,ok) while (!(ok)) goto lab

/* assertxt(test,fmt,...)
 *
 * With DEBUG undefined, assertxt() ascertains that test is valid, and it will
 * exit on error, logging a more limited error message.
 */
#define assertxt(test,fmt,...) do if (!(test)) { log_critical ("\nAssertion Failed (" fmt ")\n" , ##__VA_ARGS__); exit (-6); } while (0)

#endif /* DEBUG */


/* The platform-sensible default value for LOG_STYLE.
 * This is only used when LOG_STYLE is undefined.
 * POSIX systems normally default to syslog() style,
 * but POSIX will DEBUG to stderr by default.
 */
#ifdef _WIN32
#define LOG_STYLE_DEFAULT LOG_STYLE_STDERR
#else /* POSIX */
#ifdef DEBUG
#define LOG_STYLE_DEFAULT LOG_STYLE_STDERR
#else
#define LOG_STYLE_DEFAULT LOG_STYLE_SYSLOG
#endif /* DEBUG */
#endif /* POSIX or WIN32 */

/* Configuration option: LOG_STYLE may be set to one of:
 *  - LOG_STYLE_NULL    -- log information will be suppressed
 *  - LOG_STYLE_STDOUT  -- log information is printed to stdout
 *  - LOG_STYLE_STDERR  -- log information is printed to stderr
 *  - LOG_STYLE_SYSLOG  -- log information
 */
#define LOG_STYLE_NULL   0
#define LOG_STYLE_STDOUT 1
#define LOG_STYLE_STDERR 2
#define LOG_STYLE_SYSLOG 3

#ifndef LOG_STYLE
#define LOG_STYLE LOG_STYLE_DEFAULT
#endif

#ifdef DEBUG_DETAIL
/* Turn on DEBUG if it wasn't already */
#ifndef DEBUG
#define DEBUG
#endif
#endif


#if LOG_STYLE == LOG_STYLE_NULL

/* LOG_STYLE_NULL allows the inclusion of log_xxx statements but
 * none of the produced output is actually used.
 */

#define log_critical(fmt,...) do { ; } while (0)
#define log_warning(fmt,...)  do { ; } while (0)
#define log_error(fmt,...)    do { ; } while (0)
#define log_info(fmt,...)     do { ; } while (0)
#define log_notice(fmt,...)   do { ; } while (0)
#define log_debug(fmt,...)    do { ; } while (0)

#elif LOG_STYLE == LOG_STYLE_STDOUT

/* LOG_STYLE_STDOUT sends logging output to stdout, with preceding
 * text labels for easy recognition.
 */

#include <stdio.h>

#define log_critical(fmt,...) do { fprintf (stdout, "##\n## CRITICAL: " fmt "\n##\n" , ##__VA_ARGS__); fflush (stdout); } while (0)
#define log_error(fmt,...)    do { fprintf (stdout,           "Error: " fmt "\n"     , ##__VA_ARGS__); fflush (stdout); } while (0)
#define log_warning(fmt,...)  do { fprintf (stdout,         "Warning: " fmt "\n"     , ##__VA_ARGS__); fflush (stdout); } while (0)
#define log_info(fmt,...)     do { fprintf (stdout,                     fmt "\n"     , ##__VA_ARGS__); fflush (stdout); } while (0)
#define log_notice(fmt,...)   do { fprintf (stdout,                     fmt "\n"     , ##__VA_ARGS__); fflush (stdout); } while (0)
#define log_debug(fmt,...)    do { fprintf (stdout,           "DEBUG: " fmt "\n"     , ##__VA_ARGS__); fflush (stdout); } while (0)

#elif LOG_STYLE == LOG_STYLE_STDERR

/* LOG_STYLE_STDOUT sends logging output to stderr, with preceding
 * text labels for easy recognition.
 */

#include <stdio.h>

#define log_critical(fmt,...) do { fprintf (stderr, "##\n## CRITICAL: " fmt "\n##\n" , ##__VA_ARGS__); fflush (stderr); } while (0)
#define log_error(fmt,...)    do { fprintf (stderr,           "Error: " fmt "\n"     , ##__VA_ARGS__); fflush (stderr); } while (0)
#define log_warning(fmt,...)  do { fprintf (stderr,         "Warning: " fmt "\n"     , ##__VA_ARGS__); fflush (stderr); } while (0)
#define log_info(fmt,...)     do { fprintf (stderr,                     fmt "\n"     , ##__VA_ARGS__); fflush (stderr); } while (0)
#define log_notice(fmt,...)   do { fprintf (stderr,                     fmt "\n"     , ##__VA_ARGS__); fflush (stderr); } while (0)
#define log_debug(fmt,...)    do { fprintf (stderr,           "DEBUG: " fmt "\n"     , ##__VA_ARGS__); fflush (stderr); } while (0)

#elif LOG_STYLE == LOG_STYLE_SYSLOG

/* LOG_STYLE_SYSLOG sends logging output to the system logging service.
 */

#include <syslog.h>

#ifdef _WIN32

#error "No Windows syslogging yet"

#else /* POSIX */

#define log_critical(fmt,...) syslog (LOG_CRIT,    fmt , ##__VA_ARGS__)
#define log_error(fmt,...)    syslog (LOG_ERR,     fmt , ##__VA_ARGS__)
#define log_warning(fmt,...)  syslog (LOG_WARNING, fmt , ##__VA_ARGS__)
#define log_info(fmt,...)     syslog (LOG_INFO,    fmt , ##__VA_ARGS__)
#define log_notice(fmt,...)   syslog (LOG_NOTICE,  fmt , ##__VA_ARGS__)
#define log_debug(fmt,...)    syslog (LOG_DEBUG,   fmt , ##__VA_ARGS__)

#endif /* WIN32 / POSIX */

#else /* LOG_STYLE */

#error "No recognised LOG_STYLE setting"

#endif /* LOG_STYLE */

#include <errno.h>

/** @brief log_errno(fmt,...)
 *
 * This produces a similar output to perror(), including a string form
 * of errno.  By default, the string is formed with strerror(), but if
 * used after #include <arpa2/com_err.h> then the COM_ERR extension with
 * error_message() is used instead.  Note that this works best when all
 * error tables XXXX have been previously setup by calling the generated
 * initialize_XXXX_error_table() functions.
 */

#if defined(__COM_ERR_H) || defined (__COM_ERR_H__)
#define log_errno(fmt,...) log_error ("%s: " fmt, error_message (errno) , ##__VA_ARGS__)
#else
#define log_errno(fmt,...) log_error ("%s: " fmt,      strerror (errno) , ##__VA_ARGS__)
#endif

/** @def log_info(fmt,...)
 *
 * Produce error output.  This routes to the logging facility of
 * choice, at the info level.
 */

/** @def log_notice(fmt,...)
 *
 * Produce error output.  This routes to the logging facility of
 * choice, at the notice level.
 */

/** @def log_warning(fmt,...)
 *
 * Produce error output.  This routes to the logging facility of
 * choice, at the warning level.
 */

/** @def log_error(fmt,...)
 *
 * Produce error output.  This routes to the logging facility of
 * choice, at the error level.
 */

/** @def log_critical(fmt,...)
 *
 * Produce error output.  This routes to the logging facility of
 * choice, at the critical level.
 */

/** @def log_debug(fmt,...)
 *
 * Produce debugging output.  This routes to the logging facility of
 * choice, but only when DEBUG is defined; otherwise, no formatting
 * or logging takes place.
 */

/** @brief log_detail(fmt,...)
 *
 * Produce incredibly detailed debugging output.  For instance, the
 * choices made while parsing a new document type.  Or the bytes
 * that pop in and out of a network interface.  Only do this when
 * DEBUG_DETAIL is defined (which implies DEBUG) by passing it over
 * to the debug() code.
 */

#ifdef DEBUG_DETAIL
#define log_detail(fmt,...) log_debug (fmt , ##__VA_ARGS__)
#else
#define log_detail(fmt,...) do { ; } while (0)
#endif

/** @brief log_data(descr,ptr,len,maxsz)
 *
 * Print a packet in hexadecimal, after first printing the description
 * and an equals sign.  The entire dump is on a single line, so it is
 * easily accessible to copy/paste.  Normally the size is printed as
 * offered, but if maxsz is over zero it constrains the number of bytes
 * to that maximum.
 */

#ifdef DEBUG_DETAIL

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

static inline void log_data (const char *descr, const void *blk, uint32_t blklen, uint32_t maxsz) {
	uint32_t size = blklen;
	if ((maxsz > 0) && (size > maxsz)) {
		size = maxsz;
	}
	char buf [strlen (descr) + 3 * size + 50];
	int count = snprintf (buf, sizeof(buf), "%s =", descr);
	if (blk == NULL) {
		snprintf (buf + count, sizeof(buf) - count, "NULL");
		log_debug ("%s", buf);
		return;
	}
	for (uint32_t i=0; i < blklen; i++) {
		count += snprintf (buf + count, sizeof(buf) - count, " %02x", ((uint8_t *) blk) [i]);
		if ((size < blklen) && (i > 5) && (i == size - 5)) {
			count += snprintf (buf + count, sizeof(buf) - count,  " ...");
			i = blklen - 5;
		}
	}
	count += snprintf (buf + count, sizeof(buf) - count, " -> #%d", blklen);
	log_debug ("%s", buf);
}

#else
#define log_data(descr,blk,blklen,skip) do { ; } while (0)
#endif


#ifdef __UNREALISED_WISH__

/** @brief nok_xxx(test,fmt,...) logs when test is false;
 * usable in a common style, and see where things fail:
 *
 * bool ok = true;
 * ok = ok && nok_error (my_fun(), "my function failed!");
 * bool got_mine = ok;
 * ok = ok && nok_error (it_fun(), "it function failed!");
 * if (got_mine) { free_me (); }
 * return ok;
 *
 *
 * This style is useful to stop processing and even collect
 * intermediate "ok" results in separate variables for later
 * use, such as resources to free.  But it can be difficult
 * to find where precisely a test failed.  This makes it easy.
 *
 * It is possible to provide actual logging output to a
 * user, while at it.  If that is not useful, consider the
 * nok_debug() or even nok_detail() as these are usually
 * suppressed; especially nok_detail can help to trace a
 * problem (and on the side it add documentation to code).
 */

#if LOG_STYLE == LOG_STYLE_NULL

#define nok_critical(test,fmt,...) (test)
#define nok_warning(test,fmt,...)  (test)
#define nok_error(test,fmt,...)    (test)
#define nok_info(test,fmt,...)     (test)
#define nok_notice(test,fmt,...)   (test)
#define nok_debug(test,fmt,...)    (test)
#define nok_detail(test,fmt,...)   (test)

#else /* if not LOG_STYLE == LOG_STYLE_NULL */

#include <stdbool.h>

#define nok_critical(test,fmt,...) (test || (log_critical (fmt, ##__VA_ARGS__), false))
#define nok_warning(test,fmt,...)  (test || (log_warning  (fmt, ##__VA_ARGS__), false))
#define nok_error(test,fmt,...)    (test || (log_error    (fmt, ##__VA_ARGS__), false))
#define nok_info(test,fmt,...)     (test || (log_info     (fmt, ##__VA_ARGS__), false))
#define nok_notice(test,fmt,...)   (test || (log_notice   (fmt, ##__VA_ARGS__), false))
#define nok_debug(test,fmt,...)    (test || (log_debug    (fmt, ##__VA_ARGS__), false))
#define nok_detail(test,fmt,...)   (test || (log_detail   (fmt, ##__VA_ARGS__), false))

#endif /* LOG_STYLE == LOG_STYLE_NULL */

#endif /* __UNRELIASED_WISH__ */

#ifdef __clang__
#pragma clang diagnostic pop
#endif

#ifdef __cplusplus
}
#endif

#define _ARPA2_EXCEPT_H
#endif

/** @} */
